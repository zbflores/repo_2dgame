﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public GameObject enemy;
    Vector2 start_position;
    Vector2 position;
    public float right_waypoint;
    public float left_waypoint;
    public float posx;
    public float posy;
    public float speed;
    bool go_right = true;
    bool go_left = false;
    // Start is called before the first frame update
    void Start()
    {
        start_position.Set(11f, 5.5f);
    }

    // Update is called once per frame 
    void Update()
    {
        posx = transform.position.x;
        posy = transform.position.y;
        if (posx > right_waypoint)
        {
            go_left = true;
            go_right = false;
        }
        if (posx < left_waypoint) 
        {
            go_right = true;
            go_left = false;
        }
        if (go_right == true)
            {
            transform.Translate(speed, 0, 0);
        }
        if (go_left == true)
        {
            transform.Translate(-speed, 0, 0);
        }

    }
}
