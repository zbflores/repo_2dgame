﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class YouWin : MonoBehaviour
{
    public GameObject win_message;
    // Start is called before the first frame update
    void Start()
    {
        win_message.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        print("You have won the game");
        Invoke("Restart", 4);
        win_message.SetActive(true);

    }

    public void Restart()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
