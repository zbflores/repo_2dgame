﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement: MonoBehaviour
{
    Rigidbody2D rB2D;
    public SpriteRenderer player_SpriteRenderer;
    public Animator player_Animator;
    public float runSpeed;
    public float jumpForce;
    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    //    player_Animator = GetComponent<Animator>;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            print("Is jumping.");
            int levelMask = LayerMask.GetMask("Level_Floors");
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, 0.1f, levelMask))
            Jump();
        }
    }
    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);
        if (rB2D.velocity.x > 0)
        {
            player_SpriteRenderer.flipX = false;
        
        }
        else if (rB2D.velocity.x < 0)
        {
            player_SpriteRenderer.flipX = true;
        }
        if (Mathf.Abs(horizontalInput) > 0f)
        {
           player_Animator.SetBool("IsRunning", true);
        
        }
        else
        {
           player_Animator.SetBool("IsRunning", false);
        }
    }
    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }
}
