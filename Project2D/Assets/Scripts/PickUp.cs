﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PickUp : MonoBehaviour
{
    public GameObject pickup;
    public GameObject player;
    public TextMeshProUGUI display_score;
    static int player_score = 0;
    // Start is called before the first frame update
    void Start()
    {
        player_score = 0;
        display_score.text = "Score: 0";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        pickup.SetActive(false);
        player_score = player_score + 1;
        display_score.text = "Score: " + player_score;
        print("Player Score:" + player_score);
    }
}
